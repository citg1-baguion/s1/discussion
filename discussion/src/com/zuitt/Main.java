//directory that contains files that is used to build an application
// reverse domain notation
package com.zuitt;

// "Main" class is the entry point of a java program and is responsible for executing our code.

import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args){
//        System.out.println("Hello World");
        /*
            [SECTION] Variables
            - Syntax:
                Variable Declaration
                    datatype identifier;
                Variable Declaration and Initialization
                    dataType identifier = value;
        */

        int myNum;
        //System.out.println(myNum);

        int myNum2 = 30;
        System.out.println("Result of variable declaration and initialization:");
        System.out.println(myNum2);

//      Constants
//
        final int PRINCIPAL = 1000;
//          PRINCIPAL = 500;

        /*
        [SECTION] Primitive Data Type
        - use to store simple values
         */

        // Single Quotes (char <character>)
        // Double Quotes (string)
        char letter = 'A';
        boolean isMarried = false;

        // byte data type - due to hardware limitations memory capacity to hold specific information is limited.
        // -128 to 127 beyond -+127 error occurs
        byte students = 127;

        // short
        //-32768 to 32767
        short seats = 32767;

        //int [using {_} - underscores maybe placed in between number for code readability.
        int localPopulation = 2_147_483_647;

        //long
        // add a capital "L" in the end for long <integer>
        long worldPopulation = 7_862_081_145L;


 //     [SECTION] Float and Doubles

        //Double contains 7 decimals places
        //Float limited decimal places
        // The difference between using float and double depends on the preciseness of the values.

        float price = 12.99F;
        double temperature = 15869.8623941;


        // Checking the data type of an identifier/variable.
        System.out.println("Result of getClass Method: ");
        System.out.println(((Object)temperature).getClass());


//      [SECTION Non-primitive
        String name = "John Doe";
        System.out.println("Result of Non-primitive Data Type");
        System.out.println(name);
        String editName = name.toLowerCase();
        System.out.println(editName);

//      [SECTION] Type Casting

        //Implicit Casting -refers to automatic aversion of smaller number to larger number
        // follow larger number
        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println("Result from Implicit Casting");
        System.out.println(total);

        // Explicit Casting - refers to automatic aversion of larger number to smaller number
        int num3 = 5;
        double num4 = 2.7;
        int anotherTotal = num3 + (int)num4;
        System.out.println("Result from Explicit Casting");
        System.out.println(anotherTotal);


//      [CUT FROM Java SE Refresher]

    }
}
